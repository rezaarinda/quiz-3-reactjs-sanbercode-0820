import React, {useContext} from 'react';
import './public/css/style.css'
import logo from './public/img/logo.png'
import {MovieContext} from "../QUIZ-3//context"
import {MovieProvider} from "../QUIZ-3//context"

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import About from './about'
import Login from './login'
import MovieListEditor from './Main';

  export default function App() {
    return (
        <Router>
            <div>
                <header>
                    <img id="logo" src={logo} width="200px" />
                    <nav>
                        <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/About">About</Link>
                        </li>
                        <li>
                            <Link to="/MovieListEditor">Movie List Editor</Link>
                        </li>
                        <li>
                            <Link to="/Login">Login</Link>
                        </li>
                        </ul>
                    </nav>
                </header>
                <Switch>
                    <Route exact path="/" component={Arrange}/>
                    <Route exact path="/About" component={About}/>
                    <Route exact path="/MovieListEditor" component={MovieListEditor}/>
                    <Route exact path="/Login" component={Login}/>
                </Switch>
            </div>
        </Router>
    );
}

const Arrange = () =>{
    return(
        <MovieProvider>
            <Home/>
        </MovieProvider>
      )
}

const Home = () =>{
    const[daftarMovie] = useContext(MovieContext)

return(
    <div>
       
  <body>
    <section >
      <h1>Daftar Film Film Terbaik</h1>
      {
                daftarMovie !== null && daftarMovie.map((item, index)=>{
                    return(
                        <div id="article-list">
                            <div class="article">
                            <h3>{item.title}</h3>
                            <div style={{display:"inline-block"}}>
                                <div class="conten">
                                    <img className="image" src={item.image_url} alt="alt"/>
                                </div>
                                <div className="Info">
                                <h3>Rating {item.rating}</h3>
                                <h3>Durasi : {Math.round(item.duration/60)} jam</h3>
                                <h3>Genre : {item.genre}</h3>
                                </div>
                            </div>
                            <p>
                                {item.description}
                            </p>
                            </div>
                        </div>
                    )
                })
            }
    </section>
    <footer>
      <h5>copyright &copy; 2020 by Sanbercode</h5>
    </footer>
  </body>

    </div>
)

}
